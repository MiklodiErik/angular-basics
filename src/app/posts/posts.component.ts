import { Component } from '@angular/core';
import { PostComponent } from '../post/post.component';

type Post = {
  name: string;
  description: string;
  date: Date;
};

@Component({
  selector: 'app-posts',
  standalone: true,
  imports: [PostComponent],
  templateUrl: './posts.component.html',
  styleUrl: './posts.component.css',
})
export class PostsComponent {
  title = 'posts';
  posts: Array<Post> = [
    {
      name: 'post 1',
      description:
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Magnam molestiae dicta mollitia beatae officia. Laboriosam quos corrupti quibusdam eos, odio veritatis facere, tempora dolore facilis recusandae reprehenderit fuga aliquam fugit.',
      date: new Date('2024-02-17'),
    },
    {
      name: 'post 2',
      description:
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Magnam molestiae dicta mollitia beatae officia. Laboriosam quos corrupti quibusdam eos, odio veritatis facere, tempora dolore facilis recusandae reprehenderit fuga aliquam fugit.',
      date: new Date('2024-02-15'),
    },
    {
      name: 'post 3',
      description:
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Magnam molestiae dicta mollitia beatae officia. Laboriosam quos corrupti quibusdam eos, odio veritatis facere, tempora dolore facilis recusandae reprehenderit fuga aliquam fugit.',
      date: new Date('2024-01-10'),
    },
  ];
}
