import { Component } from '@angular/core';
import { PostsComponent } from './posts/posts.component';
import { EventsComponent } from './events/events.component';
//import { NewComponent } from './new/new.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [PostsComponent, EventsComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
})
export class AppComponent {
  title = 'root app component';
}
