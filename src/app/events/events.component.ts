import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { v4 as uuidv4 } from 'uuid';
import { Event } from '../models/event';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-events',
  standalone: true,
  imports: [FormsModule, CommonModule],
  templateUrl: './events.component.html',
  styleUrl: './events.component.css',
})
export class EventsComponent implements OnInit {
  eventTitle: string = '';
  eventDate: Date = new Date();

  events: Event[] = [];

  // runs the code block when the components initialized
  ngOnInit() {
    // load the data from local storage
    let savedEvents = localStorage.getItem('events');
    // load the data into events property
    this.events = savedEvents ? JSON.parse(savedEvents) : [];
  }

  addEvent() {
    if (this.eventTitle.length && this.eventDate) {
      let newEvent: Event = {
        id: uuidv4(),
        title: this.eventTitle,
        date: this.eventDate,
      };

      // push
      this.events.push(newEvent);
      localStorage.setItem('events', JSON.stringify(this.events));
    } else {
      console.log('fields are required');
    }
  }
  deleteEvent(index: number) {
    this.events.splice(index, 1);
    localStorage.setItem('events', JSON.stringify(this.events));
  }
}
