import { Component } from '@angular/core';

@Component({
  selector: 'app-new',
  standalone: true,
  /*template: '<p>{{componentTitle}}</p>',
  styles: `
    p {
        border: 3px solid blue;
        padding: 50px;
        background: blue;
        color: white;
    }
    `,
    */
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.css'],
})
export class NewComponent {
  componentTitle = 'this is a new component';
}
